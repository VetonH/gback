var express = require('express');
var router = express.Router();
// load the product model
var ImageC = require('../models/imagemodel');
var multer = require('multer');
var upload = multer({dest: 'public/images/'});
var mongoose = require('mongoose');
var fs = require('fs');
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });


// expose the routes to our app with module.exports
module.exports = function(app) {


//GET
                app.get('/api/images', function(req, res){
                            var imgName = req.query.name;
                            var imgBuffer = req.query.buffer;
                            var readable = req.query.read;
                            
                            if(!imgName){
                                ImageC.find({}, function(err, docs){
                                    var stringDocs = JSON.stringify(docs);
                                    var parsedDocs = JSON.parse(stringDocs);
                                    if(readable === 'True'){
                                    for(var i=0; i<parsedDocs.length; i++){ delete parsedDocs[i].image.data ;}//Removing the binary data which makes the the response unreadable
                                    res.send(parsedDocs, null, 4);
                                    }else{
                                    res.send(parsedDocs, null, 4);
                                    }
                                    
                                    
                                });
                            }
                            
                            else{
                            ImageC.findOne({'name': imgName}, function(err, docs){
                                    if (err)  throw err;
                                    var stringDocs = JSON.stringify(docs);
                                    var parsedDocs = JSON.parse(stringDocs);
                                    
                                    if (imgBuffer === 'True' || readable === 'True'){
                                    if(imgBuffer === 'True'){
                                        var imgData = parsedDocs.image.data;
                                        var image = new Buffer(imageData, 'base64');
                                        res.end(img);
                                    }else if(readable === 'True'){
                                        console.log('!!!!READABLE!!!!');
                                        for(var i=0; i<parsedDocs.length; i++){ delete parsedDocs[i].image.data; }//Removing the binary data which makes the the response unreadable
                                        res.send(parsedDocs, null, 4);
                                    }else{
                                        res.send(parsedDocs, null, 4);
                                    }
                                    
                                    }else{
                                    res.send(parsedDocs);
                                    } 
                                });
                            }
                            
                        });





//POST

                app.post('/api/images', upload.any() , function (req,res, next){
                    var fields = [];
                    var imgName;


                    if(req.files){
                        req.files.forEach(function (file){
                            console.log(file);

                                    
                            var jsonFile = JSON.stringify(file);
                            var parsedJSON = JSON.parse(jsonFile);

                            imgName = parsedJSON;

                                var path = fields[path]=parsedJSON.path;
                            
                                var imagec = new ImageC();
                                var binaryImg =  fs.readFileSync(path);
                            



                            var filename = (new Date()).valueOf()+"-"+file.originalname;
                            fs.rename(file.path, 'public/images/'+filename, function (err){
                                if(err)throw err;
                                console.log("file uploaded...");
                                    imagec.image.data = binaryImg;
                                    imagec.image.contentType = 'image/*';
                                    imagec.name = req.body.name;

                                        imagec.save(function(err, result){
                                        if(err){
                                            }
                                            res.json(result);
                                            
                                    });

                            });
                        });
                    }

            });


//DELETE                    
                // delete a paragraph
            app.delete('/api/images/:image_id', function(req, res) {
                ImageC.remove({
                    _id : req.params.image_id
                }, function(err, image) {
                    if (err)
                        res.send(err);

                    // get and return all the paragraphs after you create another
                    ImageC.find(function(err, images) {
                        if (err)
                            res.send(err);
                        res.json(images);
                    });
                });
            });



};