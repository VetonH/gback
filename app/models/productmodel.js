
    // load mongoose since we need it to define a model
    var mongoose = require('mongoose');

    var Schema=mongoose.Schema;


    var productCSchema = new Schema({
    productname: String,
    categoryname: String,
    description: String,
    nutritionalvalues: String,
    ingredients: String,
    imagepath: String


    });

    module.exports = mongoose.model('ProductC', productCSchema);

    