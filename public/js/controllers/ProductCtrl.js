(function() {

// js/controllers/main.js
angular.module('ProductCtrl', [])

    // inject the Product service factory into our controller
    .controller('ProductController', function($scope, $http, Products) {
        $scope.formData = {};

        // GET =====================================================================
        // when landing on the page, get all products and show them
        // use the service to get all the products
        Products.get()
            .success(function(data) {
                $scope.products = data;
          console.log(data);
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });

        // CREATE ==================================================================
        // when submitting the add form, send the text to the node API
        $scope.createProduct = function() {

            // validate the formData to make sure that something is there
            // if form is empty, nothing will happen
            // people can't just hold enter to keep adding the same to-do anymore
        if ($scope.formData.productname !== undefined) {

                // call the create function from our service (returns a promise object)
                Products.create($scope.formData)

                    // if successful creation, call our get function to get all the new products
                    .success(function(data) {
                        $scope.formData = {}; // clear the form so our user is ready to enter another
                        $scope.products = data; // assign our new list of products
                        console.log(data);
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
            }
    };

        // DELETE ==================================================================
        // delete a product after checking it
        $scope.deleteProduct = function(id) {
            Products.delete(id)
                // if successful creation, call our get function to get all the new products
                .success(function(data) {
                        /*

                    $scope.products.splice( id, 1);
                    // if no rows left in the array create a blank array
                    if ($scope.products.length() === 0){
                    $scope.products = [];
                    }
                            */

                    $scope.products = data; // assign our new list of products
                     console.log(data);
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
            };
    });

})();