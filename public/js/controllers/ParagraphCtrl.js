(function() {

// js/controllers/main.js
angular.module('ParagraphCtrl', [])

    // inject the Paragraph service factory into our controller
    .controller('ParagraphController', function($scope, $http, Paragraphs) {
        $scope.formParagraphData = {};

        // GET =====================================================================
        // when landing on the page, get all paragraphs and show them
        // use the service to get all the paragraphs
        Paragraphs.get()
            .success(function(data) {
                $scope.paragraphs = data;
          console.log(data);
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });

        // CREATE ==================================================================
        // when submitting the add form, send the text to the node API
        $scope.createParagraph = function() {

            // validate the formParagraphData to make sure that something is there
            // if form is empty, nothing will happen
            // people can't just hold enter to keep adding the same to-do anymore
        if ($scope.formParagraphData.content !== undefined) {

                // call the create function from our service (returns a promise object)
                Paragraphs.create($scope.formParagraphData)

                    // if successful creation, call our get function to get all the new paragraphs
                    .success(function(data) {
                        $scope.formParagraphData = {}; // clear the form so our user is ready to enter another
                        $scope.paragraphs = data; // assign our new list of paragraphs
                        console.log(data);
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
            }
    };

        // DELETE ==================================================================
        // delete a paragraph after checking it
        $scope.deleteParagraph = function(id) {
            Paragraphs.delete(id)
                // if successful creation, call our get function to get all the new paragraphs
                .success(function(data) {

                         $scope.paragraphs.splice( id, 1);
                    // if no rows left in the array create a blank array
                    if ($scope.paragraphs.length() === 0){
                    $scope.paragraphs = [];
                    }
                       

                    $scope.paragraphs = data; // assign our new list of paragraphs
                     console.log(data);
            })
            .error(function(data) {
                console.log('Error: ' + data);
            });
            };
    });

})();