			
			var path = require('path');
			
			module.exports = function(app, passport) {

			// normal routes ===============================================================

				// LOGOUT ==============================
				app.post('/logout', function(req, res) {
					req.logout();
					res.json({ redirect: '/logout' });
				});


				app.get('/api/users', isLoggedInAjax, function(req, res) {
					return res.json(req.user);
				});

				// show the home page (will also have our login links)
				app.get('', function(req, res) {
					res.sendFile(path.join(__dirname, '../public', '/views/index.html'));
					//res.sendfile('./public/views/index.html');
				});


			};

			// route middleware to ensure user is logged in - ajax get
			function isLoggedInAjax(req, res, next) {
				if (!req.isAuthenticated()) {
					return res.json( { redirect: '/login' } );
				} else {
					next();
				}
			}

			// route middleware to ensure user is logged in
			function isLoggedIn(req, res, next) {
				if (req.isAuthenticated())
					return next();

				res.redirect('/');
			}
