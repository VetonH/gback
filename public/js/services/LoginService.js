(function() {

angular.module('loginService', [])

	// super simple service
	// each function returns a promise object 
	.factory('Login', function($http) {
		return {
			create : function(userData) {
				return $http.post('login', userData);
                
			}
		};
	});

})();


    


