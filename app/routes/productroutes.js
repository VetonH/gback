// load the product model
var Product = require('../models/productmodel');
var express=require('express');
var router = express.Router();

 

var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// expose the routes to our app with module.exports
module.exports = function(app) {
    // api ---------------------------------------------------------------------
    // get all products
    app.get('/api/products', function(req, res) {

           var productname = req.query.name;
           var readable = req.query.read;

        if(!productname){
            Product.find({}, function(err, docs) {
	           var stringDocs = JSON.stringify(docs);
                var parsedDocs = JSON.parse(stringDocs);
                if(readable === 'True'){
                for(var i=0; i<parsedDocs.length; i++)//Removing the binary data which makes the the response unreadable
                res.send(parsedDocs, null, 4);
                }else{
                res.send(parsedDocs, null, 4);
                }
                
            });
        }else{
            Product.findOne({'productname': productname}, function(err, docs) {
                    if (err)  throw err;
                    var stringDocs = JSON.stringify(docs);
                    var parsedDocs = JSON.parse(stringDocs);
                    
                    if(readable === 'True'){
                        console.log('!!!!READABLE!!!!');
                        for(var i=0; i<parsedDocs.length; i++)//Removing the binary data which makes the the response unreadable
                        res.send(parsedDocs, null, 4);
                    }else if(readable === null || undefined){
                        res.send(parsedDocs, null, 4);
                    }
                    
                    else{
                    res.send(parsedDocs);
                    } 
            });
        }
    });

    // create product and send back all products after creation
    app.post('/api/products', function(req, res) {
        var productName = req.body.productname;
        var modName = productName.split(' ').join('-');
        console.log(modName);
        // create a product, information comes from AJAX request from Angular
        Product.create({
            productname : modName,
            categoryname : req.body.categoryname,
            description : req.body.description,
            nutritionalvalues : req.body.nutritionalvalues,
            ingredients : req.body.ingredients,
            imagepath : req.body.imagepath
        }, function(err, product) {
            if (err)
                res.send(err);

            // get and return all the products after you create another
            Product.find(function(err, products) {
                if (err)
                    res.send(err);
                res.json(products);
            });
        });

    });

    // delete a product
    app.delete('/api/products/:product_id', function(req, res) {
        Product.remove({
            _id : req.params.product_id
        }, function(err, product) {
            if (err)
                res.send(err);

            // get and return all the products after you create another
            Product.find(function(err, products) {
                if (err)
                    res.send(err);
                res.json(products);
            });
        });
    });
 

};
