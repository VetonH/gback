var db = require('mongoose');

var Schema = db.Schema;

var paragraphImgCSchema = new Schema({
  title: String,
  content: String,
  image: { name: String, data: Buffer, contentType: String }
});

module.exports = db.model('ParagraphImgC', paragraphImgCSchema);
