(function() {
angular.module('paragraphImgService', [])

	// super simple service
	// each function returns a promise object 
	.factory('ParagraphImgs', function($http) {
		return {
			get : function() {
				return $http.get('/api/paragraphimgs');
			},
			create : function(paragraphimageData) {
				return $http.post('/api/paragraphimgs', paragraphimageData, {
												transformRequest: angular.identity,
												headers: {
													'Content-Type': undefined

												}

											});
			},
			delete : function(id) {
				return $http.delete('/api/paragraphimgs/' + id);
			}
		};
	});


	})();