	
    // load the user model
    var Product = require('../models/usermodel');
    var express=require('express');
    var router = express.Router();

    // expose the routes to our app with module.exports
    module.exports = function(app, passport) {

        
    	// LOGIN ===============================

					// process the login form
					app.post('/login', function(req, res, next) {
						if (!req.body.username || !req.body.password) {
							return res.json({ error: 'username and Password required' });
						}
						passport.authenticate('login', function(err, user, info) {
							if (err) { 
								return res.json(err);
							}
							if (user.error) {
								return res.json({ error: user.error });
							}
							req.logIn(user, function(err) {
								if (err) {
									return res.json(err);
								}
								return res.json({ redirect: '/profile' });
							});
						})(req, res);
					});

					// process the signup form


    };
