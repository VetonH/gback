(function() {
    angular.module('profile', ['ngRoute'])
        .config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
            $routeProvider    
                .when('/profile', {
                    templateUrl: '../views/profile.html',
                    controller: 'ProfileController',
                    controllerAs: 'profile'
                }) 
                .when('/adduser', {
                    templateUrl: '../views/adduser.html',
                    controller: 'UserController',
                    controllerAs: 'profile',
                    caseInsensitiveMatch: true
                })
                .when('/addproduct', {
                    templateUrl: '../views/addproduct.html',
                    controller: 'ProductController',
                    controllerAs: 'profile'
                })
                .when('/addproductimg', {
                    templateUrl: '../views/addproductimg.html',
                    controller: 'ProductImgController',
                    controllerAs: 'profile'
                })
                .when('/addparagraphimg', {
                    templateUrl: '../views/addparagraphimg.html',
                    controller: 'ParagraphImgController',
                    controllerAs: 'profile'
                })
                .when('/addparagraph', {
                    templateUrl: '../views/addparagraph.html',
                    controller: 'ParagraphController',
                    controllerAs: 'profile'
                })
                .when('/addimage', {
                    templateUrl: '../views/addimage.html',
                    controller: 'ImageController',
                    controllerAs: 'profile'
                })
                  .when('/readme', {
                    templateUrl: '../views/readme.html',
                    controller: 'ProfileController',
                    controllerAs: 'profile'
                });

            $locationProvider.html5Mode(true);
        }])
        .controller('ProfileController', ['$http', '$scope', '$routeParams', function($http, $scope, $routeParams) {
            //Custom Profile functionality
            $http.get('/api/users')
                .success(function(data) {
                    $scope.user = data; //Expose the user data to your angular scope
                });
        }]);
  
})();


