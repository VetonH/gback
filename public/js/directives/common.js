(function(){
    angular.module('common-directives', [])
        .directive('redir', ['$http', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    element.on('click', function(e) {
                        e.preventDefault();
                        window.location = attrs.href;
                    });
                }
            };
        }])
        .directive('logout', ['$http', function($http) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    element.on('click', function(e) {
                        e.preventDefault();
                        $http.post('/logout');
                    });
                }
            };
        }])
        .directive('fileread', [function($http) {
             return {
            scope: {
                fileread: "=",
            },
            link: function (scope, element, attributes) {
                element.bind("change", function (changeEvent) {
                  scope.fileread = changeEvent.target.files[0];
		           });
                }
            };
        }]);
})();