(function() {

// js/controllers/main.js
angular.module('ProductImgCtrl', [])

            .controller('ProductImgController', function($scope, $http, ProductImgs){
                $scope.productimg = {};


                    //GET

                      ProductImgs.get()
                        .success(function(data) {

                            $scope.arrayBufferToBase64 = function( buffer ) {
                                var binary = '';
                                var bytes = new Uint8Array( buffer );
                                var len = bytes.byteLength;
                                for (var i = 0; i < len; i++) {
                                    binary += String.fromCharCode( bytes[ i ] );
                                }
                                return window.btoa( binary );
                            };

                                    $scope.productimgs = data;
                            console.log(data);
                            })
                            .error(function(data) {
                                console.log('Error: ' + data);
                            });




                        //POST

                    $scope.createProductImg = function(){

                            var formProductImgData = new FormData();
                            for(var key in $scope.productimg){
                                formProductImgData.append(key, $scope.productimg[key]);
                                
                            }

                            var file = $('#file')[0].files[0];
                            formProductImgData.append('image', file);
                            
                            
                            ProductImgs.create(formProductImgData)
                            
                                .then(function(rest){
                                    formProductImgData = {};
                                    $scope.productimg = {};
                                    $scope.productimg = res.data;

                            });
                        };



                        // DELETE ==================================================================
                        // delete a paragraph after checking it
                        $scope.deleteProductImg = function(id) {
                            ProductImgs.delete(id)
                                // if successful creation, call our get function to get all the new paragraphs
                                .success(function(data) {

                                        $scope.productimgs.splice( id, 1);
                                    // if no rows left in the array create a blank array
                                    if ($scope.productimgs.length() === 0){
                                    $scope.productimgs = [];
                                    }
                                    

                                    $scope.productimgs = data; // assign our new list of paragraphs
                                    console.log(data);
                            })
                            .error(function(data) {
                                console.log('Error: ' + data);
                            });
                            };



            });

})();