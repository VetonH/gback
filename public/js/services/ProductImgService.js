(function() {
angular.module('productImgService', [])

	// super simple service
	// each function returns a promise object 
	.factory('ProductImgs', function($http) {
		return {
			get : function() {
				return $http.get('/api/productimgs');
			},
			create : function(productimageData) {
				return $http.post('/api/productimgs', productimageData, {
												transformRequest: angular.identity,
												headers: {
													'Content-Type': undefined

												}

											});
			},
			delete : function(id) {
				return $http.delete('/api/productimgs/' + id);
			}
		};
	});


	})();