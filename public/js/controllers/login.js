(function() {
    angular.module('login', ['ngRoute'])
        .config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
            $routeProvider    
                .when('/login', {
                    templateUrl: '../../views/login.html',
                    controller: 'LoginController',
                    controllerAs: 'login',
                    caseInsensitiveMatch: true
                })
                .otherwise({
                    templateUrl: '../../views/login.html',
                    controller: 'LoginController',
                    controllerAs: 'login',
                });

            $locationProvider.html5Mode(true); //Use html5Mode so your angular routes don't have #/route
        }]);
})();
