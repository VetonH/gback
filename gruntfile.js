// Gruntfile.js
module.exports = function(grunt) {

  grunt.initConfig({

     // JS TASKS ================================================================
    // check all js files for errors
    jshint: {
      all: ['public/js/**/*.js', 'app/**/*.js', 'app/*.js', 'config/*.js'] 
    },

    // take all the js files and minify them into app.min.js
    uglify: {
      build: {
        files: {
          'public/dist/js/app.min.js': ['public/js/**/*.js',
          'config/*.js', 
          'app/**/*.js', 
          'app/*.js']
        }
      }
    },

    // CSS TASKS ===============================================================
    // process the less file to style.css

  // CSS TASKS ===============================================================
    // process the less file to style.css
    less: {
      build: {
        files: {
          'public/stylesheets/style.css': 'public/stylesheets/style.less'
        }
      }
    },

    // take the processed style.css file and minify
    cssmin: {
      build: {
        files: {
          'public/stylesheets/style.min.css': 'public/stylesheets/style.css'
        }
      }
    },

        // watch css and js files and process the above tasks
    watch: {
      css: {
        files: ['public/**/*.less'],
        tasks: ['less', 'cssmin']
      },
      js: {
        files: ['public/js/**/*.js'],
        tasks: ['jshint', 'uglify']
      }
    },

    // configure nodemon
    nodemon: {
      dev: {
        script: 'app.js'
      }
    },

        // run watch and nodemon at the same time
    concurrent: {
      options: {
        logConcurrentOutput: true
      },
      tasks: ['nodemon', 'watch']
    }   

  });


  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-nodemon');
  grunt.loadNpmTasks('grunt-concurrent');

  grunt.registerTask('default', ['less', 'cssmin', 'jshint', 'uglify', 'concurrent']);


};
