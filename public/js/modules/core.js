(function() {
    angular.module('app', ['ngRoute', 'login', 'profile', 'httpFactory', 'common-directives', 
    'angular-growl', 'ProductCtrl', 'productService', 'ParagraphCtrl', 'paragraphService' ,  
    'UserCtrl', 'userService', 'LoginCtrl', 'loginService',
    'ImageCtrl', 'imageService', 'ProductImgCtrl', 'productImgService', 'ParagraphImgCtrl', 'paragraphImgService']);

})();