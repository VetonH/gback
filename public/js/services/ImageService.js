
(function() {
angular.module('imageService', [])

	// super simple service
	// each function returns a promise object 
	.factory('Images', function($http) {
		return {
			get : function() {
				return $http.get('/api/images');
			},
			create : function(imageData) {
				return $http.post('/api/images', imageData, {
												transformRequest: angular.identity,
												headers: {
													'Content-Type': undefined

												}

											});
			},
			delete : function(id) {
				return $http.delete('/api/images/' + id);
			}
		};
	});


	})();