var express = require('express');// importing the express framework
var app = express();// naming the express object
//var mongoose = require('mongoose');// importing mongoose
var Facebook = require('facebook-node-sdk');
//var config = require('./config');
var http = require('http');
var https = require('https');
var fs = require('fs');
var util = require('util');
          
    var port     = process.env.PORT || 8000;
          var mongoose = require('mongoose');
          var passport = require('passport');
          var path     = require('path'); //Add path into our required list
          var uuid = require('node-uuid');
          var morgan       = require('morgan');
          var cookieParser = require('cookie-parser');
          var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
          var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)

          var session      = require('express-session');

          var configDB = require('./config/database.js');
          var configFB = require('./config/facebook');
          
          // configuration ===============================================================
          mongoose.connect(configDB.url); // connect to our database

//db.connect('mongodb://admin:admin123@ds139277.mlab.com:39277/webapp');
//app.set('view engine', 'ejs')// sets the view engine
          app.use(Facebook.middleware({ appId: configFB.fbOptions.appId, secret: configFB.fbOptions.appSecret }));

//app.use(Facebook.middleware({ appId: config.fbOptions.appId, secret: config.fbOptions.appSecret }));

                  require('./config/passport')(passport); // pass passport for configuration
                  app.use(function (req, res, next){
                        res.header("Access-Control-Allow-Origin", "*");
                        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, sid");
                        res.header("Access-Control-Allow-Emthods", "POST, GET, OPTIONS; DELETE; PUT");
                        next();
                    })

//Initiating all the routes
/*require('./routes/index.js')(app);
require('./routes/api/img.js')(app);
require('./routes/api/paragraph.js')(app);
require('./routes/api/fbPosts.js')(app);
require('./routes/api/product.js')(app);
*/
                   // set up our express application
                  app.use(morgan('dev')); // log every request to the console
                  app.use(cookieParser()); // read cookies (needed for auth)

                  app.use(bodyParser.urlencoded({'extended':'false'}));            // parse application/x-www-form-urlencoded
                  app.use(bodyParser.json());                                     // parse application/json
                  app.use(bodyParser.json({ type: 'application/vnd.api+json' })); 
                  app.use(methodOverride());

                  // required for passport
                  app.use(session({ secret: 'ilovescotchscotchyscotchscotch' })); // session secret
                  app.use(passport.initialize());
                  app.use(passport.session()); // persistent login sessions

                  app.use(express.static(path.join(__dirname, '/public/'))); //Expose /public



                        // routes ======================================================================
                  require('./app/index.js')(app, passport); // load our routes and pass in our app and fully configured passport
                  require('./app/routes/userroutes')(app, passport);
                  require('./app/routes/loginroutes')(app, passport);
                  
                // require('./app/fbPosts')(app);
                  require('./app/routes/paragraphroutes')(app);
                  
                  require('./app/routes/productroutes')(app);
                  require('./app/routes/productimgroutes')(app);
                  require('./app/routes/paragraphimgroutes')(app);
                  require('./app/routes/imageroutes')(app);
                  require('./app/routes/facebookroutes')(app);

                    app.get('*', function(req, res) {
  res.sendfile('./public/views/index.html')
})
//app.use('/assets/', express.static(__dirname + '/public'));
/*********************************
 * Storing tow primitives for http 
 * and https port.
 **********************************/
//var httpPort = 3000;
//var httpsPort = 443;

/*********************************
 * Options for setting a key and a
 * sertificate for an optional https 
 * server.
 **********************************/
/*var options = {
  key: fs.readFileSync('keys/key.pem'),
  cert: fs.readFileSync('keys/cert.pem')
};*/

/*********************************
 * Creating one http and one https 
 * server.
 **********************************/
//https.createServer(options, app).listen(process.env.PORT||httpsPort);
//http.createServer(app).listen(process.env.PORT||httpPort);
    // launch ======================================================================
    app.listen(port);
    console.log('The magic happens on port ' + port);


