
(function() {
angular.module('paragraphService', [])

	// super simple service
	// each function returns a promise object 
	.factory('Paragraphs', function($http) {
		return {
			get : function() {
				return $http.get('/api/paragraphs');
			},
			create : function(paragraphData) {
				return $http.post('/api/paragraphs', paragraphData);
			},
			delete : function(id) {
				return $http.delete('/api/paragraphs/' + id);
			}
		};
	});


	})();