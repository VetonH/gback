    // load mongoose since we need it to define a model
    var mongoose = require('mongoose');

    var Schema=mongoose.Schema;



    var imageCSchema = new Schema({
    name:  String,
    image:  { data: Buffer, contentType: String }
});



    module.exports = mongoose.model('ImageC', imageCSchema);

