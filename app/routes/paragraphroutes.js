// load the paragraph model
var Paragraph = require('../models/paragraphmodel');
var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// expose the routes to our app with module.exports
module.exports = function(app) {
    // api ---------------------------------------------------------------------
    // get all paragraphs
    app.get('/api/paragraphs', function(req, res) {

         var title = req.query.title;
           var readable = req.query.read;

        if(!title){
            Paragraph.find({}, function(err, docs) {


                var stringDocs = JSON.stringify(docs);
                var parsedDocs = JSON.parse(stringDocs);
                if(readable === 'True'){
                for(var i=0; i<parsedDocs.length; i++)//Removing the binary data which makes the the response unreadable
                res.send(parsedDocs, null, 4);
                }else{
                res.send(parsedDocs, null, 4);
                }
                
                
            });
        }else{
            Paragraph.findOne({'title': title}, function(err, docs) {
                if (err)  throw err;
                    var stringDocs = JSON.stringify(docs);
                    var parsedDocs = JSON.parse(stringDocs);
                    
                    if(readable === 'True'){
                        console.log('!!!!READABLE!!!!');
                        for(var i=0; i<parsedDocs.length; i++)//Removing the binary data which makes the the response unreadable
                        res.send(parsedDocs, null, 4);
                    }else if(readable === null || undefined){
                        res.send(parsedDocs, null, 4);
                    }
                    
                    else{
                    res.send(parsedDocs);
                    } 
            });
        }
    });

    // create paragraph and send back all paragraphs after creation
    app.post('/api/paragraphs', function(req, res) {

        // create a paragraph, information comes from AJAX request from Angular
        Paragraph.create({
            content : req.body.content,
            title : req.body.title,
        }, function(err, paragraph) {
            if (err)
                res.send(err);

            // get and return all the paragraphs after you create another
            Paragraph.find(function(err, paragraphs) {
                if (err)
                    res.send(err);
                res.json(paragraphs);
            });
        });

    });

    // delete a paragraph
    app.delete('/api/paragraphs/:paragraph_id', function(req, res) {
        Paragraph.remove({
            _id : req.params.paragraph_id
        }, function(err, paragraph) {
            if (err)
                res.send(err);

            // get and return all the paragraphs after you create another
            Paragraph.find(function(err, paragraphs) {
                if (err)
                    res.send(err);
                res.json(paragraphs);
            });
        });
    });
 

};