(function() {

// js/controllers/main.js
angular.module('ImageCtrl', [])

            .controller('ImageController', function($scope, $http, Images){
                $scope.image = {};


                    //GET

                      Images.get()
                        .success(function(data) {

                            $scope.arrayBufferToBase64 = function( buffer ) {
                                var binary = '';
                                var bytes = new Uint8Array( buffer );
                                var len = bytes.byteLength;
                                for (var i = 0; i < len; i++) {
                                    binary += String.fromCharCode( bytes[ i ] );
                                }
                                return window.btoa( binary );
                            };

                                    $scope.images = data;
                            console.log(data);
                            })
                            .error(function(data) {
                                console.log('Error: ' + data);
                            });




                        //POST

                    $scope.submit = function(){

                            var formData = new FormData();
                            for(var key in $scope.image){
                                formData.append(key, $scope.image[key]);
                                
                            }

                            var file = $('#file')[0].files[0];
                            formData.append('image', file);
                            
                            Images.create(formData)
                            
                                .then(function(rest){
                                    formData = {};
                                    $scope.image = {};
                                    $scope.image = res.data;

                            });
                        };



                        // DELETE ==================================================================
                        // delete a paragraph after checking it
                        $scope.deleteImage = function(id) {
                            Images.delete(id)
                                // if successful creation, call our get function to get all the new paragraphs
                                .success(function(data) {

                                        $scope.images.splice( id, 1);
                                    // if no rows left in the array create a blank array
                                    if ($scope.images.length() === 0){
                                    $scope.images = [];
                                    }
                                    

                                    $scope.images = data; // assign our new list of paragraphs
                                    console.log(data);
                            })
                            .error(function(data) {
                                console.log('Error: ' + data);
                            });
                            };



            });

})();