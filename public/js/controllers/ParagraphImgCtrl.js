(function() {

// js/controllers/main.js
angular.module('ParagraphImgCtrl', [])

            .controller('ParagraphImgController', function($scope, $http, ParagraphImgs){
                $scope.paragraphimg = {};


                    //GET

                      ParagraphImgs.get()
                        .success(function(data) {

                            $scope.arrayBufferToBase64 = function( buffer ) {
                                var binary = '';
                                var bytes = new Uint8Array( buffer );
                                var len = bytes.byteLength;
                                for (var i = 0; i < len; i++) {
                                    binary += String.fromCharCode( bytes[ i ] );
                                }
                                return window.btoa( binary );
                            };

                                    $scope.paragraphimgs = data;
                            console.log(data);
                            })
                            .error(function(data) {
                                console.log('Error: ' + data);
                            });




                        //POST

                    $scope.createParagraphImg = function(){

                            var formParagraphImgData = new FormData();
                            for(var key in $scope.paragraphimg){
                                formParagraphImgData.append(key, $scope.paragraphimg[key]);
                                
                            }

                            var file = $('#file')[0].files[0];
                            formParagraphImgData.append('image', file);
                            
                            
                            ParagraphImgs.create(formParagraphImgData)
                            
                                .then(function(rest){
                                    formParagraphImgData = {};
                                    $scope.paragraphimg = {};
                                    $scope.paragraphimg = res.data;

                            });
                        };



                        // DELETE ==================================================================
                        // delete a paragraph after checking it
                        $scope.deleteParagraphImg = function(id) {
                            ParagraphImgs.delete(id)
                                // if successful creation, call our get function to get all the new paragraphs
                                .success(function(data) {

                                        $scope.paragraphimgs.splice( id, 1);
                                    // if no rows left in the array create a blank array
                                    if ($scope.paragraphimgs.length() === 0){
                                    $scope.paragraphimgs = [];
                                    }
                                    

                                    $scope.paragraphimgs = data; // assign our new list of paragraphs
                                    console.log(data);
                            })
                            .error(function(data) {
                                console.log('Error: ' + data);
                            });
                            };



            });

})();