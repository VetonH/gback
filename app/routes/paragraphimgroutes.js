var express = require('express');
var router = express.Router();
// load the product model
var ParagraphImgC = require('../models/paragraphimgmodel');
var multer = require('multer');
var upload = multer({dest: 'public/images/'});
var mongoose = require('mongoose');
var fs = require('fs');
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });

var util = require('util');

// expose the routes to our app with module.exports
module.exports = function(app) {


//GET
                app.get('/api/paragraphimgs', function(req, res){
                            var title = req.query.title;
                            var readable = req.query.read;
                            
                            if(!title){
                                ParagraphImgC.find({}, function(err, docs){
                                    var stringDocs = JSON.stringify(docs);
                                    var parsedDocs = JSON.parse(stringDocs);
                                    if(readable === 'True'){
                                    for(var i=0; i<parsedDocs.length; i++){ delete parsedDocs[i].image.data; }//Removing the binary data which makes the the response unreadable
                                    res.send(parsedDocs, null, 4);
                                    }else{
                                    res.send(parsedDocs, null, 4);
                                    }
                                    
                                    
                                });
                            }
                            
                            else{
                            ParagraphImgC.findOne({'title': title}, function(err, docs){
                                    if (err)  throw err;
                                    var stringDocs = JSON.stringify(docs);
                                    var parsedDocs = JSON.parse(stringDocs);
                                    
                                 if(readable === 'True'){
                                        console.log('!!!!READABLE!!!!');
                                        for(var i=0; i<parsedDocs.length; i++){ delete parsedDocs[i].image.data; }//Removing the binary data which makes the the response unreadable
                                        res.send(parsedDocs, null, 4);
                                    }else if(readable === null || undefined){
                                        res.send(parsedDocs, null, 4);
                                    }
                                    
                                    else{
                                    res.send(parsedDocs);
                                    } 
                                });
                            }
                            
                        });





//POST

                app.post('/api/paragraphimgs', upload.any() , function (req,res, next){
                    var fields = [];
                    var imgName;


                    if(req.files){
                        req.files.forEach(function (file){
                            console.log(file);

                                    
                            var jsonFile = JSON.stringify(file);
                            var parsedJSON = JSON.parse(jsonFile);

                            imgName = parsedJSON;

                                var path = fields[path]=parsedJSON.path;
                                var paragraphimgc = new ParagraphImgC();
                                var binaryImg =  fs.readFileSync(path);
                            

                                var imagename = file.originalname.substr(0, file.originalname.lastIndexOf('.'));
                              

                            var filename = (new Date()).valueOf()+"-"+file.originalname;
                            fs.rename(file.path, 'public/images/'+filename, function (err){
                                if(err)throw err;



                                console.log("file uploaded...");
                                    paragraphimgc.image.data = binaryImg;
                                    paragraphimgc.image.name = imagename;
                                    paragraphimgc.image.contentType = 'image/*';
                                    paragraphimgc.title = req.body.title;
                                    paragraphimgc.content = req.body.content;

                                        paragraphimgc.save(function(err, result){
                                        if(err){
                                            }
                                            res.json(result);
                                            
                                    });

                            });
                        });
                    }

            });


//DELETE                    
                // delete a paragraph
            app.delete('/api/paragraphimgs/:paragraphimg_id', function(req, res) {
                ParagraphImgC.remove({
                    _id : req.params.paragraphimg_id
                }, function(err, paragraphimg) {
                    if (err)
                        res.send(err);

                    // get and return all the paragraphs after you create another
                    ParagraphImgC.find(function(err, paragraphimgs) {
                        if (err)
                            res.send(err);
                        res.json(paragraphimgs);
                    });
                });
            });



};